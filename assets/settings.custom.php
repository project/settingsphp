<?php
# This file is provided by https://www.drupal.org/project/settingsphp

/**
 * @file Provide custom Drupal settings.
 *
 * This file is the first extension point for settings.php.
 * See settings.includes.php for reference on the includes layout.
 *
 * If you're willing to set any setting already declared in settings.recommended.php
 * please consider using environment variables or overriding it in
 * settings.recommended-overrides.php.
 */

/**
 * If you want to redeclare $app_name or $app_env (declared in settings.init.php),
 * don't forget to redeclare also $app_instance_id.
 */
# $app_name = 'drupal';
# $app_env = 'pro';
# $app_instance_id = "{$app_name}_{$app_env}";

/**
 * Disable css/js gzip compression.
 */
#putenv('DRUPAL_CSS_GZIP', FALSE);
#putenv('DRUPAL_JS_GZIP', FALSE);
