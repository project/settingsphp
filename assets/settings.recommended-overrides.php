<?php
# This file is provided by https://www.drupal.org/project/settingsphp

/**
 * @file Provide overrides to sbitio-recommended specific Drupal settings.
 *
 * This file is the the place to override settings declared in settings.recommended.php.
 *
 * Please consider using environment variables whenever possible.
 *
 * In case you're hitting any limitation with the settings provided in settings.recommended.php
 * please fill an issue exposing your problem.
 */
