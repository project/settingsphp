<?php
# This file is provided by https://www.drupal.org/project/settingsphp

use Drupal\Core\Installer\InstallerKernel;

/**
 * @file Provide sbitio-recommended specific Drupal settings.
 *
 * Those are some of the tricks and best practices we've learned in managing
 * a variety of sites.
 *
 * Please don't edit this file. Read carefully and follow indications here
 * and in settings.custom.php if you need to override anything declared
 * in this file.
 */

/**
 * Database settings.
 *
 * Provided via environment variables or use defaults based on $app_instance_id.
 *
 * To override use settings.local.php or settings.infra.php.
 */
if (empty($databases['default']['default'])) {
  $databases['default']['default']['database'] = getenv('DB_NAME') ?: $app_instance_id;
  $databases['default']['default']['username'] = getenv('DB_USER') ?: $app_instance_id;
  $databases['default']['default']['password'] = getenv('DB_PASS') ?: $app_instance_id;
  $databases['default']['default']['host'] = getenv('DB_HOST') ?: 'localhost';
  $databases['default']['default']['port'] = getenv('DB_PORT') ?: '3306';
  $databases['default']['default']['driver'] = getenv('DB_DRIVER') ?: 'mysql';
  $databases['default']['default']['prefix'] = getenv('DB_PREFIX') ?: '';
  $databases['default']['default']['charset'] = 'utf8mb4';
  $databases['default']['default']['collation'] = 'utf8mb4_general_ci';
}

/**
 * Hash salt.
 *
 * Hash salt is considered a secret. Don't put it in the repo.
 *
 * Drupal recommendation is:
 *   «For enhanced security, you may set this variable to the contents of a file
 *    outside your document root, and vary the value across environments (like
 *    production and development); you should also ensure that this file is not
 *    stored with backups of your database.
 *
 *   $settings['hash_salt'] = file_get_contents('/home/example/salt.txt');»
 *
 * Although not secure in some scenarios, here we allow to pass the hash salt
 * via environment variable.
 *
 * ---
 *
 * You can generate the salt with:
 *
 * `drush php-eval 'echo \Drupal\Component\Utility\Crypt::randomBytesBase64(55) . "\n";'`
 *
 * or
 *
 * `dd if=/dev/urandom bs=100 count=1 2>/dev/null | base64 | cut -c 1-65 | head -n1`
 */
if (empty($settings['hash_salt']) && getenv('HASH_SALT')) {
  $settings['hash_salt'] = getenv('HASH_SALT');
}

/**
 * Deployment identifier.
 */
if (getenv('DEPLOYMENT_IDENTIFIER')) {
  $settings['deployment_identifier'] = getenv('DEPLOYMENT_IDENTIFIER');
}

/**
 * Trusted hosts.
 */
$trusted_hosts = getenv('TRUSTED_HOST_PATTERNS');
if ($trusted_hosts) {
  $settings += ['trusted_host_patterns' => []];
  $trusted_hosts = array_filter(explode(' ', $trusted_hosts));
  foreach ($trusted_hosts as $host) {
    $settings['trusted_host_patterns'][] = sprintf('^%s$', str_replace('.', '\.', $host));
  }
}

/**
 * Filesystem settings.
 */
// Create directories with the sticky bit.
$settings['file_chmod_directory'] = 02775;
// Enforce filesystem paths.
$settings['file_public_path'] = $site_path . '/files';
$settings['file_private_path'] = realpath($app_root . '/../private-files');
$settings['file_temp_path'] = realpath($app_root . '/../tmp');
$config['locale.settings']['translation']['path'] = realpath($app_root . '/../translations');

/**
 * Config settings.
 **/
if (!isset($settings['config_sync_directory'])) {
  $settings['config_sync_directory'] = $app_root . '/../config/sync';
}

/**
 * Redis.
 */
$settings['cache_prefix'] = $app_instance_id;
if ((bool) getenv('REDIS_ENABLED', FALSE)) {
  if (!InstallerKernel::installationAttempted() && extension_loaded('redis') && class_exists('Drupal\redis\ClientFactory')) {
    require $app_root . '/sites/settings.redis.php';
    $settings['container_yamls'][] = 'sites/services.redis.yml';
  }
}

/**
 * Syslog.
 */
$config['syslog.settings']['identity'] = $app_instance_id;
// Include severity in syslog message format.
$config['syslog.settings']['format'] = '!base_url|!timestamp|!type,!severity|!ip|!request_uri|!referer|!uid|!link|!message';

/**
 * Misc settings.
 */
// Disable cron autorun.
$config['automated_cron']['interval'] = 0;

// By default don't generate useless gzip aggregates.
$config['system.performance']['css']['gzip'] = (bool) getenv('DRUPAL_CSS_GZIP', TRUE);
$config['system.performance']['js']['gzip'] = (bool) getenv('DRUPAL_JS_GZIP', TRUE);

/**
 * Http client config.
 */
// Sets short timeouts for outbound connections.
// Overrideable with https://www.drupal.org/project/issues/http_client_options_per_uri module.
$settings['http_client_config']['connect_timeout'] = getenv('DRUPAL_HTTP_CLIENT_CONNECT_TIMEOUT') ?: 0.3;
$settings['http_client_config']['timeout'] = getenv('DRUPAL_HTTP_CLIENT_TIMEOUT') ?: 1;

// Set a descriptive User-Agent.
$unique_id = isset($_SERVER['HTTP_X_UNIQUE_ID']) ? $_SERVER['HTTP_X_UNIQUE_ID'] : '-';
$request_uri = isset($_SERVER['REQUEST_URI']) ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : '';
$short_uri = $request_uri ? implode('/', array_slice(explode('/', $request_uri), 0, 3)) : '/';
$amp_tag = isset($_GET['amp']) ? '?amp' : '';
$settings['http_client_config']['headers']['User-Agent'] = implode(' ', [
  $app_name . '/' . $app_env,
  gethostname(),
  $unique_id,
  $_SERVER['REQUEST_URI'],
  $short_uri . $amp_tag,
]);

/**
 * Drush specific.
 */
if (PHP_SAPI === 'cli') {
  if (getenv('DRUSH_MEMORY_LIMIT')) {
    ini_set('memory_limit', getenv('DRUSH_MEMORY_LIMIT'));
  }
}
