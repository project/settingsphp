<?php
# This file is provided by https://www.drupal.org/project/settingsphp

/**
 * @file Layout of Drupal settings includes.
 *
 * Please follow the comments and don't edit this file.
 */

/** Initialize app name and environment */
include $app_root . '/' . $site_path . '/settings.init.php';

/** Custom settings */
include $app_root . '/' . $site_path . '/settings.custom.php';

/** Recommended settings */
include $app_root . '/' . $site_path . '/settings.recommended.php';

/* Override recommended settings */
include $app_root . '/' . $site_path . '/settings.recommended-overrides.php';

/**
 * Settings managed out of the application repository.
 */
// Environment specific settings - sensitive data.
if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}

// Current deployment specific settings.
if (file_exists($app_root . '/sites/settings.deployment.php')) {
  include $app_root . '/sites/settings.deployment.php';
}

// Infrastructure specific settings.
if (file_exists($app_root . '/' . $site_path . '/settings.infra.php')) {
  include $app_root . '/' . $site_path . '/settings.infra.php';
}

// Drupal won't run if no hash salt is set.
if (empty($settings['hash_salt'])) {
  die('Missing hash_salt!');
}
