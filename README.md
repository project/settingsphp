## About

`settingsphp` is a composer plugin that provides a layout of `settings.*.php` files,
managed as [scaffold](https://www.drupal.org/docs/develop/using-composer/using-drupals-composer-scaffold) files.

The aim of this plugin is to propose an standardisation around the management of settings.php.
It has been designed to accomplish the following principles:

### 1. Keep main `settings.php` files clean.

In our vision it should be a copy of `default.settings.php` with minimal additions at the end.

This project adds a single include line to `settings.php`.

### 2. Allow configuration via environment variables.

Allowing configuration via environment variables enhances security, flexibility,
and portability by separating configuration from code, simplifying deployment,
and supporting dynamic customization.

### 3. Provide sane defaults.

By providing sane defaults we aim to reduce configuration overhead by ensuring
Drupal works out-of-the-box with minimal setup.

### 4. Provide (opinionated) best practices from sbitio.

File `settings.recommended.php` provides sbitio specific Drupal settings.

Those are some of the tricks and best practices we've learned in managing a variety of sites across the years.

### 5. Allow to extend and override everything.

Extension points to tweak and suit your needs are provided.


## Installation

To enable scaffold you must add the the configuration for `drupal/settingsphp` to the root `composer.json` file of your project:

```diff
     "extra": {
         "drupal-scaffold": {
+            "allowed-packages": [
+              "drupal/settingsphp"
+            ],
```

Then run `composer require drupal/settingsphp` and observe this output:

```
Scaffolding files for drupal/settingsphp:
  - NOTICE Modifying existing file at [web-root]/sites/default/settings.php. Examine the contents and ensure that it came out correctly.
  - Append to [web-root]/sites/default/settings.php from assets/settings.php-additions.txt
  - Copy [web-root]/sites/default/settings.includes.php from assets/settings.includes.php
  - Copy [web-root]/sites/default/settings.init.php from assets/settings.init.php
  - Copy [web-root]/sites/default/settings.custom.php from assets/settings.custom.php
  - Copy [web-root]/sites/default/settings.recommended.php from assets/settings.recommended.php
  - Copy [web-root]/sites/default/settings.recommended-overrides.php from assets/settings.recommended-overrides.php
  - Copy [web-root]/sites/settings.redis.php from assets/settings.redis.php
  - Copy [web-root]/sites/services.redis.yml from assets/services.redis.yml
```

This line is added at the end of `web/sites/default/settings.php`:

```diff
// Custom settings.
include $app_root . '/' . $site_path . '/settings.includes.php';
```

If you're installing over an existing project, now it's time to review settings.php
and remove, adapt or relocate each one of your customizations. When you're done,
replace `settings.php` with a copy of `default.settings.php` adding the include line
at the end of the file.


## Usage

Please read files carefully and add your customizations to `settings.custom.php` or `settings.recommended-overrides.php`.

Wenever possible we suggest to provide configuration via environmente variables.


## Environment variables reference.

See a reference of environment variables in `.env.example`.

### Docker compose / stack

You can use this variables file directly from your compose file with:

```yaml
services:
  drupal:
    image: $DRUPAL_IMAGE:$DRUPAL_IMAGE_TAG
    # Allow any env to be passed without having to declare it explicitly.
    env_file: .env
```
